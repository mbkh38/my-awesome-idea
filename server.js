//Das ist node als server und client:

var http = require("http");
var fs = require("fs");

http.createServer(function (req, res) {
    var findFile = function (name) {
        return fs.readFileSync(name);
    }


    switch (req.url) {
        case "/": res.end(findFile("home.html"));

            break;

        case "/impressum": res.end(findFile("impressum.html"));

            break;

        case "/datenschutzerklaerung": res.end(findFile("dsgvo.html"));

        default: res.end("could not find or open file for reading");
            break;
    }



}).listen("3000", function () {
    console.log("bound to port 3000");
})
